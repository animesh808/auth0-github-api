import React, {Component} from 'react';
const api = "https://api.github.com/users/";
class GithubProfile extends Component {
  constructor(props){
    super(props);
    this.state = {
      username: 'Animesh808',
      name: '',
      company: '',
      email: '',
      location: '',
      blog: '',
      followers: '',
      following: '',
      repos: '',
      avatar: '',
      homeUrl: '',
      notFound: ''
    };
  }

  fetchProfile(username){
    const self = this;
    let finalUrl = `${api}${username}`
    console.log(finalUrl);
    fetch(finalUrl)
    .then((res) => res.json())
    .then((data) => {
      self.setState({
        username: data.login,
        name: data.name,
        company: data.company,
        email: data.email,
        location: data.location,
        website: data.blog,
        followers: data.followers,
        following: data.following,
        repos: data.public_repos,
        avatar: data.avatar_url,
        homeUrl: data.html_url,
        notFound: data.message
      });
    })
    .catch( (error) => alert("An error occured!!! Can't fetch data"))
  }

  componentDidMount(){
    this.fetchProfile(this.state.username);
  }

  render(){
    return(
      <div>
        <div className="container">
          <h2 className='text-primary'>Hello {this.state.username}, Your github profile description is here</h2><br/><br/>
          <div className="row">
            <div className="col-md-3">
              <img src={this.state.avatar} width='200' height='200'/>
            </div>
            <div className="col-md-5">
              <h6>Name: {this.state.name}</h6>
              <h6>Email:{this.state.email}</h6>
              <h6>Company:{this.state.company}</h6>
              <h6>Location:{this.state.location}</h6>
              <h6>Website:{this.state.website}</h6>
            </div>
            <div className="col-md-4">
              <div className="row">
                <div className="col-md-4">
                  <strong>Repos: <span className="text-success">{this.state.repos}</span></strong>
                </div>
                <div className="col-md-4">
                  <strong>Followers: <span className="text-primary">{this.state.followers}</span></strong>
                </div>
                <div className="col-md-4">
                  <strong>Following: <span className="text-warning">{this.state.following}</span></strong>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default GithubProfile;
