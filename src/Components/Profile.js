import React from 'react';
import {Card, Button} from 'react-bootstrap'

import { useAuth0 } from "../react-auth0-spa";

const Profile = () => {
  const { isAuthenticated, loginWithPopup, logout, user } = useAuth0();
  return(
    <div>
      <div className="container">
        {isAuthenticated && user!=null ?
          <Card style={{ width: '18rem', align: 'center'}} className="text-center">
            <Card.Body>
              <Card.Img variant="top" src={user.picture} />
              <Card.Title>Welcome <b className='text-primary'>{user.nickname}</b></Card.Title>
              <Card.Text>
                You can view your github profile & search public repositories from this site.
              </Card.Text>
              <Button variant="danger" onClick={() => logout()}>Log Out</Button>
            </Card.Body>
          </Card>
          :
          <Card style={{ width: '18rem', align: 'center'}}>
            <Card.Body>
              <Card.Title>Hello User</Card.Title>
              <Card.Text>
                Currently yor are not logged in this system. Please login to continue.
              </Card.Text>
              <Button variant="primary" onClick={() => loginWithPopup({})}>Log In</Button>
            </Card.Body>
          </Card>
        }

      </div>
    </div>
  );
}

export default Profile;
