// src/components/NavBar.js

import React from "react";
import {Navbar, Nav, Form, Button} from 'react-bootstrap'
import { useAuth0 } from "../react-auth0-spa";
import { Link } from "react-router-dom";

const Header = () => {
  const { isAuthenticated, loginWithPopup, user, logout } = useAuth0();
  return (
    <div>
      <Navbar bg="dark" expand="lg">
        <Navbar.Brand href="#home">Github Searcher</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Link to="/">Home</Link>&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to="/profile">Profile</Link>&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to="/github-profile">Github</Link>&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to="/github-search">Github Search</Link>&nbsp;&nbsp;&nbsp;&nbsp;
          </Nav>
          <Form inline>
            {!isAuthenticated && (
              <Button variant="outline-success" onClick={() => loginWithPopup({})}>Log In</Button>
            )}

            {isAuthenticated && <Button variant="outline-danger" onClick={() => logout()}>Log Out</Button>}
          </Form>
        </Navbar.Collapse>
      </Navbar>

    </div>
  );
};

export default Header;
