import React, {Component} from 'react';

const api = "https://api.github.com/users/";

class GithubSearch extends Component {
  constructor(props){
    super(props);
    this.userRef = React.createRef();
    this.state = {
      username: '',
      name: '',
      company: '',
      email: '',
      location: '',
      blog: '',
      followers: '',
      following: '',
      repos: '',
      avatar: '',
      homeUrl: '',
      reposUrl: '',
      gistsUrl: '',
      notFound: ''
    };
  }

  fetchProfile(){
    const self = this;
    let finalUrl = `${api}${self.userRef.current.value}`;
    fetch(finalUrl)
    .then((res) => res.json())
    .then((data) => {
      self.setState({
        username: data.login,
        name: data.name,
        company: data.company,
        email: data.email,
        location: data.location,
        website: data.blog,
        followers: data.followers,
        following: data.following,
        repos: data.public_repos,
        avatar: data.avatar_url,
        homeUrl: data.html_url,
        reposUrl: data.repos_url,
        gistsUrl: data.gists_url,
        notFound: data.message
      });
    })
    .catch( (error) => alert("An error occured!!! Can't fetch data"))
  }


  render(){
    var imgStyle ={
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
      marginTop: '25px'
    }
    return(
      <div>
        <br/><br/><br/><br/>
        <div className='row'>
          <div className="col-md-3"></div>
          <div className="col-md-6">
            <input type="text" className="form-control" placeholder="Enter username" ref={this.userRef}/>
          </div>
          <div className="col-md-3"><button className="btn btn-success" onClick={this.fetchProfile.bind(this)}>Search</button></div>
        </div><br/><br/>

        <div className="row">
          <div className="col-md-4"></div>

          <div className="col-md-4">
            <div className="row bg-primary" style={{height: '150px'}}><img style={imgStyle} src={this.state.avatar} width="100" height="100" /></div>
            <div className="text-center"><h4 className="text-success">{this.state.name}</h4></div>
            <div className="row">
              <p className="col-md-6"><strong>Email: <span className="text-success">{this.state.email}</span></strong></p>
              <p className="col-md-6"><strong>Company: <span className="text-success">{this.state.company}</span></strong></p>
            </div>
            <div className="row">
              <p className="col-md-6"><strong>Website: <span className="text-success">{this.state.website}</span></strong></p>
              <p className="col-md-6"><strong>Location: <span className="text-success">{this.state.location}</span></strong></p>
            </div><hr/>

            <div className="row">
              <div className="col-md-4">
                <strong>Repos: <span className="text-success">{this.state.repos}</span></strong>
              </div>
              <div className="col-md-4">
                <strong>Followers: <span className="text-primary">{this.state.followers}</span></strong>
              </div>
              <div className="col-md-4">
                <strong>Following: <span className="text-warning">{this.state.following}</span></strong>
              </div>
            </div><br/>

            <div className="row">
              <div className="col-md-4">
                <a className="btn btn-primary" href={this.state.reposUrl}>View Repos</a>
              </div>
              <div className="col-md-4">
                <a className="btn btn-primary" href={this.state.gistsUrl}>View Gists</a>
              </div>
              <div className="col-md-4">
                <a className="btn btn-primary" href={this.state.homeUrl}>View Profile</a>
              </div>
            </div>
          </div>

          <div className="col-md-4"></div>
        </div>
      </div>
    );
  }
}

export default GithubSearch;
