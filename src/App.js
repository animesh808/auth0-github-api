import React from "react";
import Header from "./Components/Header";
import Profile from "./Components/Profile";
import GithubSearch from "./Components/GithubSearch";
import GithubProfile from "./Components/GithubProfile";
import { Router, Route, Switch } from "react-router-dom";
import { useAuth0 } from "./react-auth0-spa";
import 'bootstrap/dist/css/bootstrap.min.css';
import history from "./utils/history";
import PrivateRoute from "./Components/PrivateRoute";

function App() {
  const { isAuthenticated, loading, user } = useAuth0();

  if (!isAuthenticated && loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="App">
      <Router history={history}>
        <header>
          <Header />
        </header>
        <Switch>
          <Route path="/" exact />
          <PrivateRoute path="/profile" component={Profile} user={user} />
          <PrivateRoute path="/github-profile" component={GithubProfile} />
          <PrivateRoute path="/github-search" component={GithubSearch} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
